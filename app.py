from flask import Flask, render_template, request, redirect, session, abort, jsonify
from flask_wtf import CSRFProtect
import sqlite3
import os
import secrets

app = Flask(__name__)
app.secret_key = secrets.token_hex(16)  # Generate a random secret key

csrf = CSRFProtect(app)  # Initialize CSRF protection

@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        try:
            id = int(request.form['id'])  # convert the id to integer
        except ValueError:
            return render_template('index.html', error="Invalid ID. Please enter a valid integer ID.")

        password = request.form['password']

        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()

        cursor.execute("SELECT staff_id FROM Staff_login WHERE staff_id=? AND pw=?", (id, password))
        staff = cursor.fetchone()

        if staff:
            session['staff_id'] = staff[0]
            return redirect('/pos')

    return render_template('index.html')

@app.route('/pos')
def pos():
    if 'staff_id' not in session:
        return redirect('/')
    else:
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()

        brand = request.args.get('brand')
        name = request.args.get('name')

        query = """
            SELECT Product_info.product_id, Product_info.p_name, Product_info.p_price, Products_brand.brand_name 
            FROM Product_info 
            JOIN Products_brand ON Product_info.brand_id = Products_brand.brand_id
        """
        params = []

        if brand:
            query += " WHERE Products_brand.brand_name LIKE ?"
            params.append('%' + brand + '%')

        if name:
            if "WHERE" in query:
                query += " AND Product_info.p_name LIKE ?"
            else:
                query += " WHERE Product_info.p_name LIKE ?"
            params.append('%' + name + '%')

        cursor.execute(query, params)
        products = cursor.fetchall()

        # Get the current order's items
        if 'order_id' in session:
            cursor.execute("""
                SELECT Products_in_order.product_id, Product_info.p_name, Product_info.p_price, Products_in_order.quantity,
                (Product_info.p_price * Products_in_order.quantity) as total_price, Products_brand.brand_name
                FROM Products_in_order 
                JOIN Product_info ON Products_in_order.product_id = Product_info.product_id
                JOIN Products_brand ON Product_info.brand_id = Products_brand.brand_id
                WHERE order_id=?
            """, (session['order_id'],))
            order_items = cursor.fetchall()
        else:
            order_items = []

        # Get finalized order details from session
        order_id = None 
        total_price = None

        # Check if order was just finalized
        if 'finalized_order_id' in session:
            order_id = session['finalized_order_id']
            total_price = session['finalized_total_price']
            
            # Remove from session
            session.pop('finalized_order_id', None) 
            session.pop('finalized_total_price', None)

        error = request.args.get('error')
        return render_template('pos.html', products=products, order_items=order_items, 
                                order_id=order_id, total_price=total_price, error=error)

@app.route('/add_to_order', methods=['POST'])
def add_to_order():

  if 'staff_id' not in session:
    return redirect('/')
  
  product_id = request.form['product_id']
  quantity = int(request.form['quantity'])

  conn = sqlite3.connect('database.db')
  cursor = conn.cursor()

  cursor.execute("SELECT stock_num FROM Products_stock WHERE product_id=?", (product_id,))
  stock = cursor.fetchone()

  if stock and stock[0] >= quantity:

    if 'order_id' not in session:
      cursor.execute("INSERT INTO Customer_orders (staff_id, store_id, total_price) VALUES (?, ?, ?)", (session['staff_id'], 1, 0))
      conn.commit()
      cursor.execute("SELECT last_insert_rowid()")
      session['order_id'] = cursor.fetchone()[0]

    cursor.execute("SELECT * FROM Products_in_order WHERE order_id=? AND product_id=?", (session['order_id'], product_id))
    existing_product = cursor.fetchone()

    if existing_product:
      new_quantity = existing_product[2] + quantity
      cursor.execute("UPDATE Products_in_order SET quantity=? WHERE order_id=? AND product_id=?", (new_quantity, session['order_id'], product_id))
    else:
      cursor.execute("INSERT INTO Products_in_order (order_id, product_id, quantity) VALUES (?, ?, ?)", (session['order_id'], product_id, quantity))
    
    cursor.execute("UPDATE Products_stock SET stock_num = stock_num - ? WHERE product_id = ?", (quantity, product_id))

    conn.commit()

  else:
    return jsonify({"error": "Not enough stock for product"}), 400
  
  return "Success"

@app.route('/remove_from_order/<product_id>', methods=['GET'])
def remove_from_order(product_id):
    if 'staff_id' not in session:
        return redirect('/')
    else:
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()

        # Delete the product from the order
        cursor.execute("DELETE FROM Products_in_order WHERE order_id=? AND product_id=?", (session['order_id'], product_id))
        conn.commit()

        return redirect('/pos')

@app.route('/finalize_order', methods=['POST'])
def finalize_order():
    if 'staff_id' not in session:
        return redirect('/')
    else:
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()

        # Get the current order's items
        if 'order_id' in session:
            cursor.execute("""
                SELECT Products_in_order.product_id, Product_info.p_price, Products_in_order.quantity 
                FROM Products_in_order 
                JOIN Product_info ON Products_in_order.product_id = Product_info.product_id
                WHERE order_id=?
            """, (session['order_id'],))
            order_items = cursor.fetchall()

            total_price = 0
            for item in order_items:
                total_price += item[1] * item[2]  # price * quantity

            # Update the total price of the order
            cursor.execute("UPDATE Customer_orders SET total_price=? WHERE order_id=?", (total_price, session['order_id']))
            conn.commit()

            order_id = session['order_id']

            session.pop('order_id', None)

            # Store order_id and total_price in session
            session['finalized_order_id'] = order_id
            session['finalized_total_price'] = total_price

            return redirect('/pos')
        else:
            return render_template('pos.html', error="No order to finalize.")

@app.route('/member_form')
def member_form():
    if 'staff_id' not in session:
        return redirect('/')
    else:
        #Get the success message from the session 
        message = session.get('message', None)

        #Remove the message from the session 
        if message:
            session.pop('message', None)

        return render_template('member_form.html', message=message)

@app.route('/submit_member_form', methods=['POST'])
def submit_member_form():
    if 'staff_id' not in session:
        return redirect('/')
    else:
        username = request.form['username']
        name = request.form['name']
        phone = request.form['phone']
        email = request.form['email']
        birthday_month = request.form['birthday']
        password = request.form['password']
        confirm_password = request.form['confirm_password']

        # Compare the passwords
        if password != confirm_password:
            # You might want to handle the case when the passwords don't match
            pass

        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()

        # Insert into Customers_info table
        cursor.execute("""
            INSERT INTO Customers_info (username, c_name, c_phone, c_email, c_birthday_month) 
            VALUES (?, ?, ?, ?, ?)
        """, (username, name, phone, email, birthday_month))

        # Insert into Customer_signup table
        cursor.execute("""
            INSERT INTO Customer_signup (username, pw) 
            VALUES (?, ?)
        """, (username, password))

        # Add 500 points for new member
        cursor.execute("""
            INSERT INTO Member_Points (username, points)
            VALUES (?, 500)
        """, (username,))

        conn.commit()

        # Store the success message in the session 
        session['message'] = 'Customer information registered successfully.'

        return redirect('/member_form')

@app.route('/members_list')

def members_list():

  if 'staff_id' not in session:

    return redirect('/')

  conn = sqlite3.connect('database.db')

  cursor = conn.cursor()

  username = request.args.get('username')

  query = """SELECT c.username, c.c_name, c.c_phone, c.c_email, c.c_birthday_month, m.points 
             FROM Customers_info c 
             LEFT JOIN Member_Points m ON c.username = m.username"""

  params = []

  if username:

    query += " WHERE c.username LIKE ?"  

    params.append('%' + username + '%')

  cursor.execute(query, params)

  members = cursor.fetchall()

  return render_template('members_list.html', members=members, username=username)

@app.route('/stocks')
def stocks():
  if 'staff_id' not in session:
    return redirect('/')
  else:
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    brand = request.args.get('brand')
    name = request.args.get('name')  

    query = """
    SELECT p.product_id, p.p_name, b.brand_name, p.p_price, s.stock_num
    FROM Product_info p
    JOIN Products_brand b ON p.brand_id = b.brand_id  
    JOIN Products_stock s ON p.product_id = s.product_id
    """

    params = []

    if brand:
      query += " WHERE brand_name LIKE ?"
      params.append('%' + brand + '%')

    if name:
      if "WHERE" in query:
        query += " AND p_name LIKE ?" 
      else:
        query += " WHERE p_name LIKE ?"
      params.append('%' + name + '%')
    
    cursor.execute(query, params)
    stocks = cursor.fetchall()

    return render_template('stocks.html', stocks=stocks)

@app.route('/order_history')
def order_history():
  if 'staff_id' not in session:
    return redirect('/')
  else:
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    brand = request.args.get('brand')
    name = request.args.get('name')

    query = """
      SELECT o.order_id, o.order_datetime, po.product_id, i.p_name, i.p_price, b.brand_name, po.quantity 
      FROM Customer_orders o
      JOIN Products_in_order po ON o.order_id = po.order_id
      JOIN Product_info i ON po.product_id = i.product_id 
      JOIN Products_brand b ON i.brand_id = b.brand_id
    """

    params = []

    if brand:
      query += " WHERE b.brand_name LIKE ?"  
      params.append('%' + brand + '%')

    if name:
      if "WHERE" in query:
        query += " AND i.p_name LIKE ?"
      else:
        query += " WHERE i.p_name LIKE ?"
      params.append('%' + name + '%')
    
    cursor.execute(query, params)
    orders = cursor.fetchall()

    return render_template('order_history.html', orders=orders)

@app.route('/logout')
def logout():
    session.pop('staff_id', None)
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
