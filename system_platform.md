# System platform

## Low level:

Docker

fly.toml (settings of the program for fly.io deployment)

Alpine's python package

## High level:

Flask

# User operation guide

## Installation:

There are 3 ways to install the program, one is to deploy it on fly.io, the other way is to use any other platform that supports Dockerfile out of the box, and the final way is to install on a VPS.

For the sake of simplicity this guide is going to focus on installation on fly.io, though you can also deploy it anywhere else that supports python ad/or docker.

### Step 0

Clone the repository at https://git.disroot.org/cyi1341/pos-hw

### Step 1

Get an account at https://fly.io, follow the registration process.

### Step 2

Install `flyctl` by following through https://fly.io/docs/hands-on/install-flyctl/

### Step 3

Change your fly.io app name in fly.toml, just change the first line of the file without using any space for the name.

### Step 4

Sign in with `fly auth login`

### Step 5

Deploy the app with `fly deploy`, for the first time there will be options for the type of machine, you can scale as you see fit

### Step 6

If deployed successfully, it will prompt you a website, click on it and it will redirect you to the deployed site, to update the app you just do `fly deploy` again, and fly.io will handle everything for you.

## Usage guide

### Normal staff

For normal staff, login with the login ID and password given by the admin, and you can use the POS, the interface of the POS should be self explanatory, for more info check the README.

### Admins

For admins, you need to use `fly ssh console` inside the project folder to login to the web app, then connect to the SQL with `sqlite3 database.db`, you can then check the schema by doing `.schema` inside the SQLite database, and do various administrative tasts with it using pure SQL.

Since this is a Linux machine, you can also use it to do various Linux related tasks with it, including to add files to the project or to update the database during production, but be reminded that doing so has it's own risk.
