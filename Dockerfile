# Use smaller Python Alpine image
FROM python:3.11-alpine

# Install sqlite3 dependencies
RUN apk add --no-cache sqlite

# Copy app files
COPY . /app 

# Install app dependencies
WORKDIR /app
RUN pip install --trusted-host pypi.python.org --no-cache-dir -r requirements.txt

# Create database from schema
RUN sqlite3 database.db < base.sql

# Expose port
EXPOSE 8080

# Start app
CMD ["python", "app.py"]
