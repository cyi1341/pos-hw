CREATE TABLE Staff_info(
 staff_id INTEGER PRIMARY KEY CHECK(staff_id >= 10000 AND staff_id <= 99999),
 store_id INTEGER NOT NULL,
 s_name TEXT NOT NULL,
 s_phone INTEGER NOT NULL,
 s_email TEXT NOT NULL
);

CREATE TABLE Staff_login(
 staff_id INTEGER NOT NULL CHECK(staff_id >= 10000 AND staff_id <= 99999), 
 pw TEXT NOT NULL,
FOREIGN KEY (staff_id) REFERENCES Staff_info (staff_id)
);

CREATE TABLE Customer_orders(
 order_id INTEGER PRIMARY KEY AUTOINCREMENT,
 staff_id TEXT NOT NULL,
 store_id INTEGER NOT NULL,
 total_price REAL NOT NULL,
 order_datetime TEXT DEFAULT CURRENT_TIMESTAMP, 
FOREIGN KEY (staff_id) REFERENCES Staff_info (staff_id)
);

CREATE TABLE Products_in_order(
 order_id TEXT NOT NULL, 
 product_id INTEGER NOT NULL,
 quantity INTEGER NOT NULL,
FOREIGN KEY (order_id) REFERENCES Customer_orders(order_id)
);

CREATE TABLE Product_info(
 product_id INTEGER PRIMARY KEY AUTOINCREMENT,
 p_name TEXT NOT NULL,
 p_price INTEGER NOT NULL,
 brand_id INTEGER NOT NULL,
 category_id INTEGER NOT NULL,
 discount_id INTEGER NOT NULL,
 description TEXT NOT NULL
);

CREATE TABLE Products_brand(
 brand_id INTEGER PRIMARY KEY AUTOINCREMENT,
 brand_name TEXT NOT NULL
);

CREATE TABLE Products_category(
 category_id INTEGER PRIMARY KEY,
 category_name TEXT NOT NULL
);

CREATE TABLE Products_discount(
 discount_id INTEGER PRIMARY KEY,
 discount_type TEXT NOT NULL,
 discount_percentage INTEGER NOT NULL,
 start_date TEXT NOT NULL,
 end_date TEXT NOT NULL
);

CREATE TABLE Products_stock(
 product_id INTEGER NOT NULL,
 store_id INTEGER NOT NULL,
 stock_num INTEGER NOT NULL,
FOREIGN KEY (product_id) REFERENCES Product_info(product_id)
);

CREATE TABLE Store_info(
 store_id INTEGER PRIMARY KEY AUTOINCREMENT,
 store_name TEXT NOT NULL,
 store_address TEXT NOT NULL
);

CREATE TABLE Customers_info (
 username TEXT PRIMARY KEY,
 c_name TEXT NOT NULL,
 c_phone INTEGER NOT NULL,
 c_email TEXT NOT NULL,
 c_birthday_month INTEGER NOT NULL
);

CREATE TABLE Customer_signup(
 username TEXT NOT NULL,
 pw TEXT NOT NULL,
FOREIGN KEY(username) REFERENCES Customers_info(username)
);

CREATE TABLE Member_Points(
 username TEXT NOT NULL,
 points INTEGER NOT NULL,
 FOREIGN KEY(username) REFERENCES Customers_info(username)
);


INSERT INTO Products_category (category_id, category_name) VALUES (1, 'tops');
INSERT INTO Products_category (category_id, category_name) VALUES (2, 'bottoms');
INSERT INTO Products_category (category_id, category_name) VALUES (3, 'footwear');
INSERT INTO Products_category (category_id, category_name) VALUES (4, 'headwear');
INSERT INTO Products_category (category_id, category_name) VALUES (5, 'handwear');
INSERT INTO Products_category (category_id, category_name) VALUES (6, 'equipment');
INSERT INTO Products_category (category_id, category_name) VALUES (7, 'accessories');

INSERT INTO Staff_info (staff_id, s_name, store_id, s_email, s_phone) VALUES (10001, 'mary joe', 1, 'mary@example.com', 87654321);
INSERT INTO Staff_info (staff_id, s_name, store_id, s_email, s_phone) VALUES (10002, 'john doe', 1, 'john@example.com', 12345678);
INSERT INTO Staff_info (staff_id, s_name, store_id, s_email, s_phone) VALUES (10003, 'peter hoe', 0, 'peter@example.com', 12345432);

INSERT INTO Staff_login (staff_id, pw) VALUES (10001, '87654321');
INSERT INTO Staff_login (staff_id, pw) VALUES (10002, '12345678');
INSERT INTO Staff_login (staff_id, pw) VALUES (10003, '12345432');

INSERT INTO Product_info (p_name, p_price, brand_id, category_id, discount_id, description) VALUES ('Football', 100, 1, 6, 1, 'Standard size football');
INSERT INTO Product_info (p_name, p_price, brand_id, category_id, discount_id, description) VALUES ('Basketball Jersey', 50, 2, 1, 1, 'Basketball jersey for men');
INSERT INTO Product_info (p_name, p_price, brand_id, category_id, discount_id, description) VALUES ('Running Shoes', 150, 3, 3, 1, 'Running shoes for women');

INSERT INTO Products_brand (brand_name) VALUES ('Nike');
INSERT INTO Products_brand (brand_name) VALUES ('Adidas');
INSERT INTO Products_brand (brand_name) VALUES ('Puma');

INSERT INTO Products_category (category_name) VALUES ('Sportswear');
INSERT INTO Products_category (category_name) VALUES ('Footwear');
INSERT INTO Products_category (category_name) VALUES ('Equipment');

INSERT INTO Products_stock (product_id, store_id, stock_num) VALUES (1, 1, 100);
INSERT INTO Products_stock (product_id, store_id, stock_num) VALUES (2, 1, 200);
INSERT INTO Products_stock (product_id, store_id, stock_num) VALUES (3, 1, 150);

INSERT INTO Store_info (store_name, store_address) VALUES ('Sports Central', '123 Central Street, Hong Kong');
INSERT INTO Store_info (store_name, store_address) VALUES ('Sports East', '456 East Road, Hong Kong');
INSERT INTO Store_info (store_name, store_address) VALUES ('Sports West', '789 West Avenue, Hong Kong');

INSERT INTO Customers_info 
VALUES ('user1', 'user1', 12345678, 'user1@example.com', 3);

INSERT INTO Customers_info
VALUES ('user2', 'different name', 87654321, 'user2@example.com', 6);

INSERT INTO Customer_signup 
VALUES ('user1', 'password1');

INSERT INTO Customer_signup
VALUES ('user2', 'password2');

INSERT INTO Member_Points (username, points)
VALUES ('user1', 300);

INSERT INTO Member_Points (username, points)
VALUES ('user2', 200);
