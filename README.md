# POS System Documentation

## Overview

This is a web-based point of sale (POS) system for a sports store called Marathon Sports. It is built using Python Flask and SQLite.

The system has the following features:

- Staff login
- Product search and sale
- Member registration 
- View member list
- View product stocks
- View order history

## Folder Structure

```
pos-hw
├── app.py
├── base.sql
├── Dockerfile 
├── fly.toml
├── requirements.txt
├── icons
├── static
│   ├── layout.css
│   ├── manifest.json
│   ├── member_form.css 
│   ├── order_history.css
│   ├── pos.css
│   ├── serviceworker.js  
│   ├── stocks.css
│   └── style.css
└── templates
    ├── index.html
    ├── layout.html
    ├── member_form.html
    ├── members_list.html
    ├── order_history.html 
    ├── pos.html
    └── stocks.html
```

## Main Files

- `app.py`: The main Flask application
- `base.sql`: SQLite database schema and initial data
- `requirements.txt`: Python dependencies 
- `Dockerfile`: For containerizing the app
- `fly.toml`: Fly.io configuration
- `static/`: Folder containing CSS stylesheets
- `templates/`: Folder containing HTML templates

## Key Functions

**app.py**

- `login()`: Login page and authentication
- `pos()`: Main POS page to search/add products 
- `add_to_order()`: Add product to current order
- `finalize_order()`: Finalize order
- `member_form()`: Display new member registration form
- `submit_member_form()`: Submit new member form
- `members_list()`: Display list of members
- `stocks()`: View product stocks
- `order_history()`: View past orders

## Frontend Components

The main frontend pages and their purposes:

- `index.html`: Login page
- `layout.html`: Base layout 
- `pos.html`: Main POS interface
- `member_form.html`: New member registration form
- `members_list.html`: Display members list
- `stocks.html`: Display product stocks
- `order_history.html`: Display past orders

## Styling and JavaScript

Most pages use a common `layout.css` stylesheet for styling navbar and sidebar components. Additional stylesheets exist for individual pages.

Custom JavaScript exists in `pos.js` to:

- Dynamically add/update products in current order
- Communicate with backend to add to order
- Display errors
